package com.mona.mybilling;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;

public class MainActivity extends AppCompatActivity  implements BillingProcessor.IBillingHandler{

    BillingProcessor bp;
    CardView btnPurchase, btnAbone, btnAbone1, btnAbone2;
    TextView txtDisplay;
    private TransactionDetails purchaseTransactionDetails = null;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bp = new BillingProcessor(this, getString(R.string.licance_key), this);
        bp.initialize();


        btnPurchase = findViewById(R.id.btnPurchase);
        btnAbone = findViewById(R.id.btnAbone);
        btnAbone1 = findViewById(R.id.btnAbone1);
        btnAbone2 = findViewById(R.id.btnAbone2);

        txtDisplay = findViewById(R.id.txtDisplay);


    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {

    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {

    }
    private boolean hasSubscription() {

        if (purchaseTransactionDetails != null) {
            return purchaseTransactionDetails.purchaseInfo != null;
        }
        return false;
    }
    @Override
    public void onBillingInitialized() {
        Log.d("MainActivity", "onBillingInitialized: ");

        String premium = getResources().getString(R.string.full);
        String onemounth = getResources().getString(R.string.onemounth);
        String threemounth = getResources().getString(R.string.threemount);
        String sixmounth = getResources().getString(R.string.sixmount);

        purchaseTransactionDetails = bp.getSubscriptionTransactionDetails(premium);

        bp.loadOwnedPurchasesFromGoogle();

        btnPurchase.setOnClickListener(v -> {
            if (bp.isOneTimePurchaseSupported()) {
                bp.purchase(this, premium);
            } else {
                Log.d("MainActivity", "onBillingInitialized: Subscription updated is not supported");
            }
        });
        btnAbone.setOnClickListener(v -> {
            if (bp.isSubscriptionUpdateSupported()) {
                bp.subscribe(this, onemounth);
            } else {
                Log.d("MainActivity", "onBillingInitialized: Subscription updated is not supported");
            }
        });
        btnAbone1.setOnClickListener(v -> {
            if (bp.isSubscriptionUpdateSupported()) {
                bp.subscribe(this, threemounth);
            } else {
                Log.d("MainActivity", "onBillingInitialized: Subscription updated is not supported");
            }
        });
        btnAbone2.setOnClickListener(v -> {
            if (bp.isSubscriptionUpdateSupported()) {
                bp.subscribe(this, sixmounth);
            } else {
                Log.d("MainActivity", "onBillingInitialized: Subscription updated is not supported");
            }
        });


        Log.d("info",purchaseTransactionDetails.purchaseInfo.responseData);
        if (hasSubscription()) {
            txtDisplay.setText("Status: Premium");
        } else {
            txtDisplay.setText("Status: Free");
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }
}